module.exports = function(RED) {

    const request = require('request-promise-native');

    // The servers below don't bundle the intermediate certificate along with the server certificate, which leads to
    // "unable to verify the first certificate" errors. That's why we use HTTP instead of HTTPS.
    const DailyOpsUri  = 'https://data.wra.gov.tw/Service/OpenData.aspx?format=json&id=50C8256D-30C5-4B8D-9B84-2E14D5C6DF71';
    const ConditionUri = 'https://data.wra.gov.tw/Service/OpenData.aspx?format=json&id=1602CA19-B224-4CC3-AA31-11B1B124530F';

    const EnableLog = false;

    function log() {
        if(EnableLog) {
            arguments[0] = '[' + new Date().toISOString() + '] ' + arguments[0];
            console.log.apply(console, arguments);
        }
    }

    // Returns msg['_test'][key] if it exists and is non-null, otherwise returns defaultValue.
    function getTestParam(msg, key, defaultValue) {
        return '_test' in msg && key in msg['_test'] && msg['_test'][key] !== undefined
             ? msg['_test'][key]
             : defaultValue;
    }

    // Combines the Daily Operational Statistics with the Condition Data.
    // The returned data is a 1:n mapping from the former to the latter because the Condition Data
    // contains multiple data points (with different time stamps) per reservoir.
    // The data is indexed using the human-readable reservoir name.
    //
    // Example data:
    // {
    //     "阿公店水庫" : {
    //         "ReservoirName": '阿公店水庫',
    //         "DeadStorageLevel": '27',
    //         "OutflowTotal": '1.99',
    //         "FullWaterLevel": '37',
    //         "Outflow": '1.99',
    //         "RecordTime": '2020-02-07T00:00:00',
    //         "ReservoirIdentifier": '30802',
    //         "EffectiveCapacity": '1522.54',
    //         "RegulatoryDischarge": '',
    //         ...
    //         "ConditionData": [
    //             {
    //                 "ObservationTime": '2020-02-07T23:00:00',
    //                 "WaterLevel": '35.23',
    //                 "EffectiveWaterStorageCapacity": '1037.655',
    //                 "AccumulateRainfallInCatchment": '0',
    //                 "ReservoirIdentifier": '30802',
    //                 "DesiltingTunnelOutflow": '',
    //                 ...
    //             },
    //             {
    //                 "ObservationTime": '2020-02-08T00:00:00'
    //                 "WaterLevel": '35.23',
    //                 ...
    //             },
    //             ...
    //         ]
    //     },
    //     ...
    // }
    //
    function combineData(dailyOps, condition) {
        log('Combining data');
        var data = dailyOps.reduce(function (acc, cur) {
            acc[cur.ReservoirName] = cur;
            acc[cur.ReservoirName]['ConditionData'] = condition.filter(
                elem => (elem.ReservoirIdentifier === cur.ReservoirIdentifier)
            );
            return acc;
        }, {});
        return data;
    }

    function requestJsonAsync(uri, rootKey) {
        log('Requesting', uri, 'and extracting root element', rootKey);
        return request(uri)
            .then(body => {
                log('Body received from', uri + ':', body.length, 'bytes');
                //log("===============================================================================\n",
                //  body.toString('utf8'),
                //  "\n==============================================================================="
                //);
                // We use trim() below to remove any BOM that may be present:
                // https://stackoverflow.com/questions/44176194
                return JSON.parse(body.toString('utf8').trim())[rootKey]
            })
            .catch(err => {
                const errStr = `Failed to request/parse '${uri}': ${err}`;
                log('ERROR: ', errStr);
                throw errStr;
            });
    }

    function getTwOpendataReservoirs(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function(msg, send, done) {
            send = send || function() { node.send.apply(node, arguments) }  // Node-RED 0.x compatibility

            // Note the 'node-red:' prefix to escape to the top-level namespace.
            // This is documented here: https://nodered.org/docs/creating-nodes/i18n
            // Messages can be found in: @node-red/nodes/locales/en-US/messages.json
            node.status({ fill: "blue", shape: "dot", text: "node-red:httpin.status.requesting" });

            const dailyOpsUri  = getTestParam(msg, 'dailyOpsUri',  DailyOpsUri);
            const conditionUri = getTestParam(msg, 'conditionUri', ConditionUri);
            Promise.all([
                requestJsonAsync(dailyOpsUri,  'DailyOperationalStatisticsOfReservoirs_OPENDATA'),
                requestJsonAsync(conditionUri, 'ReservoirConditionData_OPENDATA'),
            ]).then(function (values) {
                /*
                msg.payload = {
                    'dailyOps'  : values[0],
                    'condition' : values[1],
                };
                */
                node.status({});
                msg.payload = combineData(values[0], values[1]);
                send(msg);
                if(done) {
                    done();                 // Node-RED 1.0 compatible
                }
            }).catch(function (err) {
                log('ERROR: One or more requests have failed.');
                node.status({ fill: "red", shape: "ring", text: err.toString() });
                if(done) {
                    done(err);              // Node-RED 1.0 compatible
                }
                else {
                    node.error(err, msg);   // Node-RED 0.x compatible
                }
            });

        });
    }

    RED.nodes.registerType("tw-opendata-reservoirs", getTwOpendataReservoirs);
}
