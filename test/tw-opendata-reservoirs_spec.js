// See:
// - https://github.com/node-red/node-red-node-test-helper
// - https://github.com/node-red/node-red/wiki/Testing

const should = require('should');
const helper = require('node-red-node-test-helper');
const myNode = require('../tw-opendata-reservoirs.js');
const request = require('request-promise-native');
const jsonata = require('jsonata');

helper.init(require.resolve('node-red'));

describe('tw-opendata-reservoirs Node', function () {

    beforeEach(function (done) {
        helper.startServer(done);
    });

    afterEach(function (done) {
        // Calling unload() crashes if a test never called helper.load().
        // Detect that situation by testing for internals.
        // Other people have had the same problem but it remains unresolved for now:
        // https://github.com/node-red/node-red-node-test-helper/issues/38
        if(helper._logSpy) {
            //console.log('afterEach: unloading and stopping server');
            helper.unload().then(function() {
                helper.stopServer(done);
            });
        }
        else {
            //console.log('afterEach: stopping server only');
            helper.stopServer(done);
        }
    });

    it('empty tests should work', function (done) {
        done();
    });

    it('requests should work', function (done) {
        this.timeout(5000);
        request('https://httpbin.org/encoding/utf8')
            .then(body => {
                const response = body.toString('utf8');
                response.should.not.be.empty();
                done();
            });
    });

    it('should load', function (done) {
        var flow = [{ id: "n1", type: "tw-opendata-reservoirs", name: "tw-opendata-reservoirs" }];
        helper.load(myNode, flow, function () {
            var n1 = helper.getNode("n1");
            n1.should.have.property('name', 'tw-opendata-reservoirs');
            done();
        });
    });

    var nodeMsgOutput = null;

    it('should return a well-formed payload', function (done) {
        this.timeout(5000);
        var flow = [
            { id: "n1", type: "tw-opendata-reservoirs", name: "tw-opendata-reservoirs", wires: [["n2"]] },
            { id: "n2", type: "helper" }
        ];
        helper.load(myNode, flow, function () {
            var n2 = helper.getNode("n2");
            var n1 = helper.getNode("n1");
            n2.on("input", function (msg) {
                try{
                    msg.should.have.property('payload');

                    const p = msg.payload;
                    p.should.have.property('寶山第二水庫');
                    p.should.have.property('鳳山水庫');

                    const r = p.寶山第二水庫;
                    r.should.have.property('ReservoirIdentifier').which.is.a.String();
                    r.should.have.property('FullWaterLevel').which.is.a.String();
                    r.should.have.property('ConditionData').which.is.a.Array().which.is.not.empty();

                    const cd = r.ConditionData[0];
                    cd.should.have.property('ReservoirIdentifier').which.is.a.String();
                    cd.should.have.property('WaterLevel').which.is.a.String();
                    cd.should.have.property('ObservationTime').which.is.a.String();

                    // Cheat a bit and save the payload as input for the next test
                    nodeMsgOutput = msg;
                    //console.log(JSON.stringify(p));

                    done();
                }
                catch(err) {
                    // The on() method swallows test exceptions, so pass them directly to Mocha's done handler.
                    // https://stackoverflow.com/questions/16607039
                    done(err);
                }
            });
            n1.receive({ payload: "ping" });
        });
    });

    it('should return a payload suitable for JSONata', function (done) {
        // This assumes the payload was saved by the previous test
        should.exist(nodeMsgOutput);
        //console.log(JSON.stringify(nodeMsgOutput));

        const exp = jsonata(`
        (
            $number_or_null := function($a) {
                  $a = "" ? null : $number($a)
            };
            payload.[阿公店水庫].{
                $.ReservoirName: {
                    "EffectiveCapacity": $number_or_null(EffectiveCapacity),
                    "DeadStorageLevel": $number_or_null(DeadStorageLevel),
                    "FullWaterLevel": $number_or_null(FullWaterLevel),
                    "ConditionData": ConditionData.{
                        "ObservationTime": ObservationTime,
                        "ObservationTimeMillis": $toMillis(ObservationTime),
                        "WaterLevel": $number_or_null(WaterLevel),
                        "EffectiveWaterStorageCapacity": $number_or_null(EffectiveWaterStorageCapacity)
                    }
                }
            }
        )
        `);

        let result;
        try {
            result = exp.evaluate(nodeMsgOutput);
            //console.log(JSON.stringify(result, null, '  '));
        }
        catch(err) {
            console.log('ERROR: Unable to evaluate JSONata expression:', err);
            done(err);
        }

        result.should.have.property('阿公店水庫');

        const r = result.阿公店水庫;
        r.should.have.property('EffectiveCapacity').which.is.a.Number();
        r.should.have.property('DeadStorageLevel').which.is.a.Number();
        r.should.have.property('FullWaterLevel').which.is.a.Number();
        r.should.have.property('ConditionData').which.is.a.Array().which.is.not.empty();

        const cd = r.ConditionData[0];
        cd.should.have.property('ObservationTime').which.is.a.String();
        cd.should.have.property('ObservationTimeMillis').which.is.a.Number();
        cd.should.have.property('WaterLevel').which.is.a.Number();
        cd.should.have.property('EffectiveWaterStorageCapacity').which.is.a.Number();

        done();
    });

    it('should handle request errors', function (done) {
        this.timeout(5000);
        var flow = [
            { id: "n1", type: "tw-opendata-reservoirs", name: "tw-opendata-reservoirs", wires: [["n2"]] },
            { id: "n2", type: "helper" }
        ];
        helper.load(myNode, flow, function () {
            var n1 = helper.getNode("n1");
            var n2 = helper.getNode("n2");
            n2.on("input", function (msg) {
                try{
                    //console.log('received msg:', msg);
                    should.fail(msg, undefined, 'no output message expected');
                }
                catch(err) {
                    done(err);
                }
            });
            n1.receive({
                payload: "ping",
                _test: {
                    dailyOpsUri: 'https://httpbin.org/status/500',
                    conditionUri: 'https://httpbin.org/status/500',
                },
            });
            n1.on('call:error', function(call) {
                call.should.be.called();
                done();
            });
        });
    });

});
